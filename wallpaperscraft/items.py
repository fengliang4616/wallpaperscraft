# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class WallpaperscraftItem(scrapy.Item):
    catalog = scrapy.Field()  # 图片类别
    url = scrapy.Field()  # 下载URL
