# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import csv


class WallpaperscraftPipeline:
    def __init__(self):
        self.file = open('wallpaper.csv', 'a+', encoding="utf-8", newline='')
        self.writer = csv.writer(self.file)
        self.writer.writerow(['catalog', 'url'])

    def process_item(self, item, spider):
        if item['url']:
            self.writer.writerow([item['catalog'], item['url']])
        return item

    def close_spider(self, spider):
        self.file.close()
