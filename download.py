# -*- coding: utf-8 -*-
"""
# @Author : FengLiang
# @Time : 2020/6/11 11:22
# @File : download.py
"""

import os
import time
import aiohttp
import asyncio
import nest_asyncio
import pandas as pd

nest_asyncio.apply()

root = 'wallpaper'
if not os.path.exists(root):
    os.makedirs(root)


async def job(session, catalog, url):
    name = str(url).split('/')[-1]
    img = await session.get(url)
    content = await img.read()
    with open(os.path.join(root, catalog, name), 'wb') as f:
        f.write(content)


async def main(loop):
    async with aiohttp.ClientSession() as session:
        for catalog in list(df.catalog.unique()):
            start = time.time()
            path = os.path.join(root, catalog)
            if not os.path.exists(path):
                os.makedirs(path)
            tmp_df = df[df.catalog == catalog]
            urls = list(tmp_df.url)

            tasks = [
                loop.create_task(job(session, catalog, url)) for url in urls
            ]
            await asyncio.wait(tasks)

            end = time.time()
            print('used time: ', end - start)


if __name__ == '__main__':
    try:
        df = pd.read_csv('wallpaper.csv')
        loop = asyncio.get_event_loop()
        loop.run_until_complete(main(loop))
        loop.close()
    except Exception as e:
        pass
