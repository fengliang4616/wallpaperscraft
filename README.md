# 说明
- 使用Scrapy爬取壁纸的下载URL保存到csv文件
- 目标网站：`https://wallpaperscraft.com/`

# 运行
- 启动爬虫：`scrapy crawl wallpaper`
- 下载图片：执行`download.py`